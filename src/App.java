import model.Person;

public class App {
    public static void main(String[] args) throws Exception {
        // Khởi tạo các thể hiện (instance) của class Person
        Person person1 = new Person();
        Person person2 = new Person("Devcamp", "User", "Male", "DU", "du@devcamp.edu.vn");

        System.out.println(person1.getFirstName());
        System.out.println(person2.getFirstName());

        person1.setFirstName("Update");

        System.out.println(person1.getFirstName());
        System.out.println(person2.getFirstName());

        System.out.println(person1.toString());
        System.out.println(person2.toString());
    }
}
