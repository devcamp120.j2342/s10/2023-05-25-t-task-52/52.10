package model;

public class Person {
    private String firstName;
    private String lastName;
    private String gender;
    private String contact;
    private String email;

    // Phương thức getter & setter
    // Chuột phải => Source Action => Generate Getters and Setters => Chọn thuộc tính
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public String getContact() {
        return contact;
    }
    public void setContact(String contact) {
        this.contact = contact;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    // Phương thức khởi tạo không tham số
    // Chuột phải => Source Action => Generate Constructors => Không chọn thuộc tính nào
    public Person() {
    }

    // Phương thức khởi tạo có tất cả các tham số
    // Chuột phải => Source Action => Generate Constructors => Chọn thuộc tính
    public Person(String firstName, String lastName, String gender, String contact, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.contact = contact;
        this.email = email;
    }  
}
